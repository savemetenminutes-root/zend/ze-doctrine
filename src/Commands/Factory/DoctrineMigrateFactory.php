<?php

namespace Smtm\ZeDoctrine\Commands\Factory;

use Interop\Container\ContainerInterface;
use Smtm\ZeDoctrine\Commands\DoctrineMigrate;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\Console\Output\ConsoleOutput;

class DoctrineMigrateFactory
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $outputInterface = new ConsoleOutput();
        $logger = new ConsoleLogger($outputInterface);
        $config = $container->get('config')['doctrine'] ?? [];

        return new DoctrineMigrate($logger, $config);
    }
}
