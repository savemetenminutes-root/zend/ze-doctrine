<?php

namespace Smtm\ZeDoctrine\Commands;

use Doctrine\DBAL\Connection;
use Doctrine\Migrations\Configuration\Configuration;
use Doctrine\Migrations\Tools\Console\Command\DiffCommand;
use Doctrine\Migrations\Tools\Console\Command\DumpSchemaCommand;
use Doctrine\Migrations\Tools\Console\Command\ExecuteCommand;
use Doctrine\Migrations\Tools\Console\Command\GenerateCommand;
use Doctrine\Migrations\Tools\Console\Command\LatestCommand;
use Doctrine\Migrations\Tools\Console\Command\MigrateCommand;
use Doctrine\Migrations\Tools\Console\Command\RollupCommand;
use Doctrine\Migrations\Tools\Console\Command\StatusCommand;
use Doctrine\Migrations\Tools\Console\Command\UpToDateCommand;
use Doctrine\Migrations\Tools\Console\Command\VersionCommand;
use Doctrine\Migrations\Tools\Console\ConsoleRunner;
use Doctrine\Migrations\Tools\Console\Helper\ConfigurationHelper;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\HelperSet;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Output\OutputInterface;

class DoctrineMigrate extends Command
{
    protected $logger;
    protected $config;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        LoggerInterface $logger,
        array $config = []
    ) {
        $this->logger = $logger;
        $this->config = $config;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('doctrine:migrate')
            // the short description shown while running "php bin/console list"
            ->setDescription('Command to load aggregated Zend configuration and proxy to a Doctrine migration command.')
            // the full command description shown when running the command with
            // the "--help" option
            //->setHelp('Could write some useful text here')

            //->addArgument('doctrineMigrationCommand', InputArgument::OPTIONAL,
            //    'The actual Doctrine migration command to run')
            ->addOption(
                'connectionKey',
                'c',
                InputOption::VALUE_REQUIRED,
                'A Doctrine connection configuration key from the aggregated Zend configuration at $config[\'doctrine\'][\'connection\'][$connectionKey]. Default value "default"',
                'default'
            )
            ->addOption(
                'migrationsKey',
                'm',
                InputOption::VALUE_REQUIRED,
                'A Doctrine migrations configuration key from the aggregated Zend configuration at $config[\'doctrine\'][\'migrations_configuration\'][$migrationsConfigurationKey]. Default value "default"',
                'default'
            )
            ->addOption(
                'args',
                'a',
                InputOption::VALUE_REQUIRED,
                'The Doctrine migration command arguments to pass - must have single or double quotes around',
                ''
            )
        ;

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        //$this->logger->info('This is some info.');
        //$this->logger->error('This is an error!');

        /*
        $doctrineMigrationCommand = $input->getArgument('doctrineMigrationCommand');
        if (empty($doctrineMigrationCommand)) {
            $doctrineMigrationCommand = 'list';
        }
        */
        $doctrineMigrationCommandArguments = $input->getOption('args');

        $connectionKey           = $input->getOption('connectionKey');
        $connectionConfiguration = $this->config['connection'][$connectionKey]['params'];
        $connectionDriver        = $this->config['connection'][$connectionKey]['driverClass'] ?? null;
        $connectionDriver        =
            is_string($connectionDriver) && class_exists($connectionDriver)
                ? new $connectionDriver()
                : new Driver();
        $connection              = new Connection(
            $connectionConfiguration,
            $connectionDriver
        );

        $migrationsKey           = $input->getOption('migrationsKey');
        $migrationsConfiguration = $this->config['migrations_configuration'][$migrationsKey] ?? [];

        $configuration = new Configuration($connection);
        $configuration->setMigrationsDirectory($migrationsConfiguration['directory']);
        $configuration->setName($migrationsConfiguration['name']);
        $configuration->setMigrationsNamespace($migrationsConfiguration['namespace']);
        $configuration->setMigrationsTableName($migrationsConfiguration['table']);

        $helperSet = new HelperSet(
            [
                'configuration' => new ConfigurationHelper($connection, $configuration),
            ]
        );
        $helperSet->set(new QuestionHelper(), 'question');

        $commands = [];
        $cli = ConsoleRunner::createApplication($helperSet, $commands);
        ConsoleRunner::addCommands($cli);

        //$migrationCommand = $cli->find($doctrineMigrationCommand);
        $newInput         = new StringInput($doctrineMigrationCommandArguments);
        /*
        return $migrationCommand->run(
            $newInput,
            $output
        );
        */
        return $cli->run($newInput, $output);
    }
}
