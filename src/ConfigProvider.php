<?php

declare(strict_types=1);

namespace Smtm\ZeDoctrine;

use Smtm\ZeDoctrine\Commands\DoctrineMigrate;
use Smtm\ZeDoctrine\Commands\Factory\DoctrineMigrateFactory;
use Smtm\ZeDoctrine\Exception\NoLocalConfigurationException;

class ConfigProvider
{
    public function __invoke() : array
    {
        return [
            'dependencies' => $this->getDependencies(),
            'doctrine' => $this->getDoctrine(),
            'commands' => $this->getCommands(),
        ];
    }

    public function getDependencies()
    {
        return [
            'factories' => [
                \Doctrine\ORM\EntityManager::class  => \ContainerInteropDoctrine\EntityManagerFactory::class,
                DoctrineMigrate::class => DoctrineMigrateFactory::class,
            ],
        ];
    }

    public function getDoctrine()
    {
        $localConfigFilename = __DIR__ . '/../../../../config/autoload/doctrine.local.php';
        if(! file_exists($localConfigFilename)) {
            $localConfigFilename = __DIR__ . '/../config/doctrine.local.php';
        }
        if(! file_exists($localConfigFilename)) {
            throw new NoLocalConfigurationException('Local configuration file found neither at ' . realpath(__DIR__ . '/../../../../config/autoload/doctrine.local.php') . ' nor at ' . realpath(__DIR__ . '/../config/doctrine.local.php'));
        }

        return [
            /*
            'driver' => [
                'default' => [
                    'class' => \Doctrine\Common\Persistence\Mapping\Driver\MappingDriverChain::class,
                    'drivers' => [],
                ],
            ],
            */
            'migrations_configuration' => [
                'default' => [
                    'directory' => __DIR__ . '/../../../../data/db/migrations',
                    'name'      => 'Doctrine Database Migrations - App',
                    'namespace' => 'App\Migration',
                    'table'     => 'migrations_default'
                ],
            ],
            'connection' => require $localConfigFilename,
        ];
    }
    
    public function getCommands()
    {
        return [
            Commands\DoctrineMigrate::class,
        ];
    }
}
